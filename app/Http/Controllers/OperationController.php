<?php

namespace App\Http\Controllers;

use App\info;
use App\order;
use App\package;
use App\person;
use App\type;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
class OperationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $package=package::all();
        $person=person::all();
        $type=type::all();

        $data=[
            'package'=>$package,
            'person'=>$person,
            'type'=>$type,

        ];
        return view('welcome',$data);
        
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $order=new order();
        $order->package_id=$request->package;
        $order->person_id=$request->person;
        $order->type_id=$request->type;
        $order->amount=$request->amount;
        $order->save();
        return redirect('/');
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detailsByPackage(Request $request) {
        $id = $request->input('id');
        $person = $request->input('person');
       
        $type = $request->input('type');
        // echo "id".$id."<br/>";
        // echo "person".$person."<br/>";
        // echo "type".$type."<br/>";
        // exit();
        $amount = DB::table('infos')->where([
            ['package_id', '=', $id],
            ['people_id', '=', $person],
            ['type_id', '=', $type],
        ])->get();
        $data = json_encode(array($amount));
        return json_decode($data);
    }
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
