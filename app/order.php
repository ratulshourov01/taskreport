<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class order extends Model
{
    protected $fillable=['package_id','person_id','type_id','amount'];
    //
}
