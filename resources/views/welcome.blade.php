<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>B2B</title>
        

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        
    <div class="content-body">
    <form class="form" action="{{ route('store') }}" method="POST" >
    @csrf
    <input type="hidden" value="{{csrf_token()}}" name="_token"/>
    <div class="form-group col-md-4">
        <label><b>Package</b></label>
        <select class="form-control" id="package_id" name="package">
        <option value="">Select a Package</option>
             @foreach ($package as  $key=>$package)
                 <option value="{{ $package->id }}"> {{ $package->name }}</option>
                    @endforeach
                    </select>
                <span class="help-block text-danger col-md-offset-4">{{ $errors->first('package') }}</span>
                <label><b>Person</b></label>
        <select class="form-control" id="person_id" name="person">
        <option value="">Select  person</option>
             @foreach ($person as  $key=>$person)
                 <option value="{{ $person->id }}"> {{ $person->person }}</option>
                    @endforeach
                    </select>
                <span class="help-block text-danger col-md-offset-4">{{ $errors->first('person') }}</span>
                <label><b>Type</b></label>
                <select class="form-control" id="type_id" name="type">
          
        <option value="">Select  Type</option>
             @foreach ($type as  $key=>$type)
                 <option value="{{ $type->id }}"> {{ $type->type }}</option>
                    @endforeach
                    </select>
                <span class="help-block text-danger col-md-offset-4">{{ $errors->first('type') }}</span>
<input type="text" name="amount" id="amount_id" readonly/>
                <button class="btn btn-primary" type="submit">Save</button>        
            </div>
            
    </form>
    </div>
    </body>
    <script type="text/javascript">
    var person;
    var type;
    var id;
     $('#person_id').on('change', function () {

 person= $('#person_id').val();
console.log("person",person);
     });
 $('#package_id').on('change', function () {

 id = $('#package_id').val();
var type= $('#type_id').val();

//  $('#brandName').val('');
// alert(id);
 });
 $('#type_id').on('change', function () {

type= $('#type_id').val();
console.log("person",person);
if (id !== "" ) {
                
                // $('#sales_contract_id').empty();
                $.ajax({
                    method: 'GET', // Type of response and matches what we said in the route
                    url: '{{ route('detailsByInfo') }}', // This is the url we gave in the route
                    data: {
                        "_token": "{{ csrf_token() }}",
                        'id': id,
                        'person':person,
                        'type':type
                    }, // a JSON object to send back
                    success: function (data) { // What to do if we succeed
                        console.log(data[0][0].amount);
                        $("#amount_id").val(data[0][0].amount);

                        
                    },
                    error: function (jqXHR, textStatus, errorThrown) { // What to do if we fail
                       
                    }
                });
            }

    });
    </script>
</html>
